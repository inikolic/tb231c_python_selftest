# Solutions

_Do Not_ look at the solutions before you did a serious attempt to solve the excercises yourself (includiung reading documentation and searching on the internet). By looking at the solutions you are giving yourself a false selse of how good you understand the topics and are reducing the learning effect.
