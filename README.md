# tb231c_python_selftest
Python programming skills self-assessment exercises for the  tb231c course

This self-assesment is offered as a extra-curricular service to help you prepare and get the most out of the tb231c course. You do not have to do this self-assesment in order to do the course, it is completely optional. Being able to do these tasks with ease (while using internet reference material) means that you will have the necessary python programming skills for taking part in tb231c course. If you have struggled with programming in previous modelling courses in the 1st year of the TBM Bsc program, you are strongly advised to use the self-assesment and the refresher as a guide for self-study.


## How to use the self assesment 
 * *Do Not look at the solution beforehand!*
 * Download Self_assesment.ipynb and do the tasks. 
   * You should be able to do this within 1 hour. It is OK if it takes longer, as long as you understand what is going on.
   * You are free to use internet to figure things out and to read python documentation.
   * Do not give up too fast!
 * You got stuck, now what ?
   * Download Refresher_extensive.ipynb and do the exercises, even if you think you get it.
   * Note at which topic you got stuck, use the materials provided in the first year python course, the Think Python book  and internet resources to develop your skills further.
 * I did all of them correctly!
  * Excellent, congratulations, you should have no python related problems in the tb231c course, and will be able to focus on learning about Agent Based Modelling

# FAQ
 * *Do I have to do this self-assesment to take part in the course?*
   * No, this is entirely optional.
 * *DO I get bonus points for the exam, or can skip lessons if I did this?*
   * No, this is an optional extra-curricular activity meant to help you upgrade your python skills.
 * *Can I ask the teachers for tb231c for help or explanation during the break / outside the course*
   * There is no extra support provided. Please use this material  as a self-study guide.
